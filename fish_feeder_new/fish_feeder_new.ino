//Written by C.J.Wijethunga
//Started 17/01/2017
//version 1.30

//from top +
//         -
//to arduino rx
//to arduino tx

 
int FEED = 0;
/*get the current time from rtc then feed the fish at a desired time*/

#include <Servo.h>
#include <RealTimeClockDS1307.h>

#define Display_Clock_Every_N_Seconds 1   // n.secs to show date/time
int count = 0;



char formatted[] = "00-00-00 00:00:00x";

int Min, Sec, Hou = 0;

int HOUR = 21 , MIN = 00; //feeding time

unsigned long  bl1 = 0; //blinking bulb

int manFeed = 0; // manual Switch
int lowFood = 0; //food level indicator

int Buz = 11; //buzzer pin
int On = 13; //on bulb
int LowBulb = 12; //food level bulb
int vibrateMotor = 8;

int feedingTime, fedCount, fedTime = 0; //initialize feeding, fed or not, last fed time


int servoPower = 10;
int servoPin = 9;
Servo Servo1;

void setup() {

  Serial.begin(9600);

  pinMode(servoPower, OUTPUT); // make servo position 0
  Servo1.attach(servoPin);
  digitalWrite(servoPower, HIGH);
  Servo1.write(0);
  delay(1000);
  digitalWrite(servoPower, LOW);

  pinMode(On, OUTPUT);
  pinMode(Buz, OUTPUT);
  pinMode(LowBulb, OUTPUT);
  pinMode(vibrateMotor, OUTPUT);
}

void loop() {
  Serial.println("");
  Serial.print("FEED=");
  Serial.print( FEED);
  Serial.println("");

  RTC.readClock();
  Hou = RTC.getHours();
  Min = RTC.getMinutes();
  Sec = RTC.getSeconds();
  Serial.println(Sec);

  lowFoodIndicate();
  printTime();
  bulbBlink();

  digitalWrite(Buz, LOW);

  if (Serial.available() > 0) {
    char command = Serial.read();
    int in, in2;
    switch (command)
    {
      case 'H':
      case 'h':
        in = SerialReadPosInt();
        RTC.setHours(in);
        RTC.setClock();
        break;
      case 'I':
      case 'i':
        in = SerialReadPosInt();
        RTC.setMinutes(in);
        RTC.setClock();
        break;
      case 'S':
      case 's':
        in = SerialReadPosInt();
        RTC.setSeconds(in);
        RTC.setClock();
        break;
      case 'Y':
      case 'y':
        in = SerialReadPosInt();
        RTC.setYear(in);
        RTC.setClock();
        break;
      case 'M':
      case 'm':
        in = SerialReadPosInt();
        RTC.setMonth(in);
        RTC.setClock();
        break;
      case 'D':
      case 'd':
        in = SerialReadPosInt();
        RTC.setDate(in);
        RTC.setClock();
        break;
      case 'z':
        RTC.start();
        break;
      case 'Z':
        RTC.stop();
        break;
    }
  }

  if ( abs(Hou - fedTime) > 5) { //set fedCount to 0 after 5 hours from last feed
    fedCount = 0;
  }

  //Serial.println( millis());
  //Serial.println(manFeed);

  //Serial.println( fedCount);
  int manFeed = analogRead(A0);
  // Serial.println(manFeed);
  if ( fedCount == 0 && manFeed > 500) { //manually feeding
    feedingTime == 1; //stop blinking
    feeding();
  }



  if (Hou == HOUR && Min == MIN && 00 < Sec && Sec < 04 && fedCount == 0) { //feed at HOUR:MIN
    feedingTime == 1; //stop blinking
    feeding();
  }


}


void printTime() {
  count++;
  if (count % Display_Clock_Every_N_Seconds == 0) {
    Serial.print(count);
    Serial.print(": ");
    RTC.getFormatted(formatted);
    Serial.print(formatted);
    Serial.println();
  }
}


void bulbBlink() {
  if (feedingTime == 0 && !RTC.isStopped()) {

    while (millis() - bl1 < 100) {
      digitalWrite(On, HIGH);
    }

    while ((millis() - bl1) < 1500) {
      digitalWrite(On, LOW);
    }
    bl1 = millis();
  }
  else {
    digitalWrite(On, HIGH);
  }

}


void  feeding() {
  FEED++;
  fedCount = 1; //set fed count
  fedTime = Hou;
  feedingTime = 0;
  digitalWrite(On, HIGH);
  digitalWrite(Buz, LOW);
  digitalWrite(Buz, HIGH);
  delay(100);
  digitalWrite(Buz, LOW);
  delay(100);
  digitalWrite(Buz, HIGH);
  delay(100);
  digitalWrite(Buz, LOW);
  delay(1000);
  digitalWrite(On, LOW);

  //digitalWrite(Buz,HIGH);
  digitalWrite(vibrateMotor, HIGH);
  delay(500);
  digitalWrite(servoPower, HIGH);
  Servo1.write(180);
  digitalWrite(vibrateMotor,HIGH);
  delay(200);
  Servo1.write(0);
  delay(1000);
  digitalWrite(servoPower, LOW);
  delay(1000);
  digitalWrite(vibrateMotor,LOW);


  //digitalWrite(Buz,LOW);
  //delay(1000);
  digitalWrite(On, HIGH);
  digitalWrite(Buz, HIGH);
  delay(100);
  digitalWrite(Buz, LOW);
  delay(100);
  digitalWrite(Buz, HIGH);
  delay(100);
  digitalWrite(Buz, LOW);
  delay(1000);
  RTC.start();

}



void lowFoodIndicate() {

  int lowFood = analogRead(A1);
  Serial.println("");
  Serial.print("low food value=");
  Serial.print( lowFood);
  Serial.println("");
  if (lowFood > 500) { //read the ldr value and indicate
    digitalWrite(LowBulb, HIGH);
    if (Min == 45 && 00 < Sec && Sec < 04) {
      digitalWrite(Buz, HIGH);
      delay(1000);
      digitalWrite(Buz, LOW);
      delay(2000);
      digitalWrite(Buz, HIGH);
      delay(1000);
      digitalWrite(Buz, LOW);
    }
  }

  else {
    digitalWrite(LowBulb, LOW);
  }

}

int SerialReadPosInt() {
  int i = 0;
  boolean done = false;
  while (Serial.available() && !done)
  {
    char c = Serial.read();
    if (c >= '0' && c <= '9')
    {
      i = i * 10 + (c - '0');
    }
    else
    {
      done = true;
    }
  }
  return i;
}



